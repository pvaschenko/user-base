<?php

namespace ZendDbMigrations\Migrations;

use ZendDbMigrations\Library\AbstractMigration;
use Zend\Db\Metadata\MetadataInterface;

class Version20160405191536 extends AbstractMigration {
    
    public function up(MetadataInterface $schema){
        $this->addSql('CREATE SEQUENCE qualification_id_seq');
        $this->addSql('CREATE TABLE education
        (
            qualification_id integer NOT NULL DEFAULT nextval(\'qualification_id_seq\'),
            name character varying(50),
            CONSTRAINT pk_qualification_id PRIMARY KEY (qualification_id)
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE education
          OWNER TO postgres;');
        $this->addSql('CREATE SEQUENCE city_id_seq');
        $this->addSql('CREATE TABLE city
        (
          city_id integer NOT NULL DEFAULT nextval(\'city_id_seq\'),
          name character varying(50),
          CONSTRAINT pk_city_id PRIMARY KEY (city_id)
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE city
          OWNER TO postgres;');
        $this->addSql('CREATE SEQUENCE user_id_seq');
        $this->addSql('CREATE TABLE users
        (
            user_id integer NOT NULL DEFAULT nextval(\'user_id_seq\'),
            name character varying(50),
            qualification_id integer NOT NULL,
            CONSTRAINT pk_user_id PRIMARY KEY (user_id),
            CONSTRAINT fk_qualification_id FOREIGN KEY (qualification_id)
              REFERENCES education (qualification_id) MATCH SIMPLE
              ON UPDATE CASCADE ON DELETE CASCADE
        )
        WITH (
            OIDS=FALSE
        );
        ALTER TABLE users
          OWNER TO postgres;');

        $this->addSql('CREATE TABLE users_city
        (
          user_id integer NOT NULL,
          city_id integer NOT NULL,
          CONSTRAINT pk_user_city PRIMARY KEY (user_id, city_id),
          CONSTRAINT fk_city_id FOREIGN KEY (city_id)
              REFERENCES city (city_id) MATCH SIMPLE
              ON UPDATE CASCADE ON DELETE CASCADE,
          CONSTRAINT fk_user_id FOREIGN KEY (user_id)
              REFERENCES users (user_id) MATCH SIMPLE
              ON UPDATE CASCADE ON DELETE CASCADE
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE users_city
          OWNER TO postgres;');
    }
    
    public function down(MetadataInterface $schema){
        $this->addSql('DROP TABLE users_city;');
        $this->addSql('DROP TABLE users;');
        $this->addSql('DROP TABLE education;');
        $this->addSql('DROP TABLE city;');
    }
}