<?php

namespace ZendDbMigrations\Migrations;

use ZendDbMigrations\Library\AbstractMigration;
use Zend\Db\Metadata\MetadataInterface;

class Version20160406144349 extends AbstractMigration {
    
    public function up(MetadataInterface $schema){
        $this->addSql("INSERT INTO city (city_id,name) VALUES (DEFAULT,'Абакан'),(DEFAULT,'Владимир'),(DEFAULT,'Вологда'),(DEFAULT,'Волгоград'),(DEFAULT,'Казань')
                      ,(DEFAULT,'Калининград'),(DEFAULT,'Москва'),(DEFAULT,'Санкт-Петербург');");
        $this->addSql("INSERT INTO education (qualification_id,name) VALUES (DEFAULT,'Среднее'),(DEFAULT,'Бакалавр'),(DEFAULT,'Магистр');");
    }
    
    public function down(MetadataInterface $schema){
        //$this->addSql(/*Sql instruction*/);
    }
}