<?php

namespace ZendDbMigrations\Migrations;

use ZendDbMigrations\Library\AbstractMigration;
use Zend\Db\Metadata\MetadataInterface;

class Version20160406175406 extends AbstractMigration {
    
    public function up(MetadataInterface $schema){
        $this->addSql("INSERT INTO users (user_id,name,qualification_id) VALUES (DEFAULT,'Иванов Иван Иванович',1);");
        $this->addSql("INSERT INTO users_city (user_id,city_id) VALUES (1,1);");
    }
    
    public function down(MetadataInterface $schema){
        //$this->addSql(/*Sql instruction*/);
    }
}