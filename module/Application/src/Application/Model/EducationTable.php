<?php
/**
 * Created by PhpStorm.
 * User: Dimon
 * Date: 07.04.2016
 * Time: 22:05
 */

namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;

class EducationTable {
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getEducation($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('qualification_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveEducation(Education $education)
    {
        $data = array(
            'name' => $education->name,
        );

        $id = (int) $education->qualification_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getEducation($id)) {
                $this->tableGateway->update($data, array('qualification_id' => $id));
            } else {
                throw new \Exception('Album id does not exist');
            }
        }
    }

    public function deleteEducation($id)
    {
        $this->tableGateway->delete(array('qualification_id' => (int) $id));
    }
} 