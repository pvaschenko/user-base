<?php
/**
 * Created by PhpStorm.
 * User: Dimon
 * Date: 07.04.2016
 * Time: 22:05
 */

namespace Application\Model;


class City {
    public $city_id;
    public $name;

    public function exchangeArray($data)
    {
        $this->city_id     = (!empty($data['city_id'])) ? $data['city_id'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
    }
} 