<?php
/**
 * Created by PhpStorm.
 * User: Dimon
 * Date: 07.04.2016
 * Time: 23:40
 */

namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;

class UsersCityTable {
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getUsersCity($id, $city = null)
    {
        $id  = (int) $id;
        $city  = (int) $city;
        if (!$city) {
            $rowset = $this->tableGateway->select(array('user_id' => $id));
        }
        else {
            $rowset = $this->tableGateway->select(array('user_id' => $id, 'city_id' => $city));
        }
        $row = $rowset;
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveUsersCity(UsersCity $usersCity)
    {
        $data = array(
            'user_id' => $usersCity->user_id,
            'city_id' => $usersCity->city_id,
        );

        $id = (int) $usersCity->user_id;
        $city = (int) $usersCity->city_id;
        if (!$this->getUsersCity($id, $city)->count()) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getUsersCity($id, $city)->count()) {
                $this->tableGateway->update($data, array('user_id' => $id));
            } else {
                throw new \Exception('UsersCity id does not exist');
            }
        }
    }

    public function deleteUsersCity($id)
    {
        $this->tableGateway->delete(array('user_id' => (int) $id));
    }
} 