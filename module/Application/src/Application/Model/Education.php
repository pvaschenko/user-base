<?php
/**
 * Created by PhpStorm.
 * User: Dimon
 * Date: 07.04.2016
 * Time: 22:05
 */

namespace Application\Model;


class Education {
    public $qualification_id;
    public $name;

    public function exchangeArray($data)
    {
        $this->qualification_id     = (!empty($data['qualification_id'])) ? $data['qualification_id'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
    }
} 