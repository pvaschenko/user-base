<?php
/**
 * Created by PhpStorm.
 * User: Dimon
 * Date: 07.04.2016
 * Time: 22:06
 */

namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;

class CityTable {
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getCity($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('city_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveCity(City $city)
    {
        $data = array(
            'name' => $city->name,
        );

        $id = (int) $city->city_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getCity($id)) {
                $this->tableGateway->update($data, array('city_id' => $id));
            } else {
                throw new \Exception('Album id does not exist');
            }
        }
    }

    public function deleteCity($id)
    {
        $this->tableGateway->delete(array('city_id' => (int) $id));
    }
} 