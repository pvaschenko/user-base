<?php
/**
 * Created by PhpStorm.
 * User: Dimon
 * Date: 07.04.2016
 * Time: 20:51
 */

namespace Application\Model;


class Users {
    public $user_id;
    public $name;
    public $qualification_id;

    public function exchangeArray($data)
    {
        $this->user_id     = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->qualification_id  = (!empty($data['qualification_id'])) ? $data['qualification_id'] : null;
    }
} 