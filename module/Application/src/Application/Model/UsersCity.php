<?php
/**
 * Created by PhpStorm.
 * User: Dimon
 * Date: 07.04.2016
 * Time: 23:26
 */

namespace Application\Model;


class UsersCity {
    public $city_id;
    public $user_id;

    public function exchangeArray($data)
    {
        $this->city_id = (!empty($data['city_id'])) ? $data['city_id'] : null;
        $this->user_id = (!empty($data['user_id'])) ? $data['user_id'] : null;
    }
} 