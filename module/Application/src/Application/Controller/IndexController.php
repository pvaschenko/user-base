<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Model\Users;
use Application\Model\UsersCity;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractRestfulController
{
    public $adapter;
    protected $usersTable;
    protected $cityTable;
    protected $educationTable;
    protected $usersCityTable;

    public function indexAction()
    {
        $education = $this->params()->fromQuery('education');
        $cities = $this->params()->fromQuery('city');
        if ($cities || $education) {
            return $this->getFilter($cities, $education);
        }
        return $this->getList();
    }

    public function createAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            return $this->create($data);
        } else {
            return new JsonModel(array(
                'data' => 'No data sent.',
            ));
        }
    }

    public function updateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $id = $this->params()->fromRoute('id');
            return $this->update($id,$data);
        } else {
            return new JsonModel(array(
                'data' => 'No data sent.',
            ));
        }
    }

    public function cityAction()
    {
        $results = array();
        foreach($this->getCityTable()->fetchAll() as $key => $city) {
            $results[$key] = get_object_vars($city);
            $results[$key]['id'] = $results[$key]['city_id'];
            unset($results[$key]['city_id']);
        }
        return new JsonModel(array(
            'data' => $results,
        ));
    }

    public function educationAction()
    {
        $results = array();
        foreach($this->getEducationTable()->fetchAll() as $key => $education) {
            $results[$key] = get_object_vars($education);
            $results[$key]['id'] = $results[$key]['qualification_id'];
            unset($results[$key]['qualification_id']);
        }
        return new JsonModel(array(
            'data' => $results,
        ));
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        return $this->delete($id);
    }

    public function getAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        return $this->get($id);
    }

    public function getList()
    {
      $results = $this->getUsersTable()->fetchAll();
      $users = array();
      foreach($results as $key=>$result) {
          $users[$key] = get_object_vars($result);
          $users[$key]['q_id'] = $users[$key]['qualification_id'];
          $users[$key]['id'] = $users[$key]['user_id'];
          unset($users[$key]['qualification_id']);
          unset($users[$key]['user_id']);
          foreach($this->getEducationTable()->fetchAll() as $education) {
              if ($education->qualification_id == $users[$key]['q_id']) {
                  $users[$key]['qualification'] = $education->name;
              }
          }
          $cities = array();
          $citiesArr = array();
          $cities = $this->getUsersCityTable()->getUsersCity($users[$key]['id']);
          foreach($cities as $user_city) {
              foreach ($this->getCityTable()->fetchAll() as $city) {
                  if ($city->city_id == $user_city->city_id) {
                      $citiesArr[] = $city->name;
                  }
              }
          }
          $users[$key]['city'] = implode(',',$citiesArr);
      }

      return new JsonModel(array(
          'data' => $users,
      ));
    }

    public function getFilter($cities_in,$education)
    {
        $users = array();
        if ($education) {
            $results = $this->getUsersTable()->fetchAll();
            if (strpos($education,',')!==false) {
                $education = explode(',',$education);
            }
            foreach($results as $key=>$result) {
                $users[$key] = get_object_vars($result);
                $users[$key]['q_id'] = $users[$key]['qualification_id'];
                $users[$key]['id'] = $users[$key]['user_id'];
                unset($users[$key]['qualification_id']);
                unset($users[$key]['user_id']);
                foreach ($this->getEducationTable()->fetchAll() as $educations) {
                    if ($educations->qualification_id == $users[$key]['q_id']) {
                        $users[$key]['qualification'] = $educations->name;
                    }
                }
                if ((is_array($education) && !in_array((string)$users[$key]['q_id'], $education))
                    || (!is_array($education) && $users[$key]['q_id'] != $education)) {
                    unset($users[$key]);
                }
            }
        } else {
            $results = $this->getUsersTable()->fetchAll();
            foreach($results as $key=>$result) {
                $users[$key] = get_object_vars($result);
                $users[$key]['q_id'] = $users[$key]['qualification_id'];
                $users[$key]['id'] = $users[$key]['user_id'];
                unset($users[$key]['qualification_id']);
                unset($users[$key]['user_id']);
                foreach ($this->getEducationTable()->fetchAll() as $educations) {
                    if ($educations->qualification_id == $users[$key]['q_id']) {
                        $users[$key]['qualification'] = $educations->name;
                    }
                }
            }
        }
        $results = $users;
        foreach($results as $key => $result) {
            $user_cities = $this->getUsersCityTable()->getUsersCity($result['id'],null);
            $citiesArr = array();
            $searchArr = array();
            foreach ($user_cities as $city) {
                $citiesArr[] =  $this->getCityTable()->getCity($user_cities->current()->city_id)->name;
                $searchArr[] = $user_cities->current()->city_id;
            }
            $results[$key]['city'] = implode(',',$citiesArr);
            if ($cities_in) {
                if (!is_array($cities_in) && strpos($cities_in,',')===false) {
                    if (!in_array($cities_in, $searchArr)) {
                        unset($results[$key]);
                    }
                } else {
                    if (!is_array($cities_in)){
                        $cities_in = explode(',', $cities_in);
                    }
                    $notfound = true;
                    foreach($cities_in as $city) {
                        if (in_array($city, $searchArr)) {
                            $notfound = false;
                        }
                    }
                    if ($notfound) unset($results[$key]);
                }
            }
        }
        return new JsonModel(array(
            'data' =>  array_values($results),
        ));
    }

    public function get($id)
    {
        $user = $this->getUsersTable()->getUsers($id);
        $user = get_object_vars($user);
        $user['q_id'] = $user['qualification_id'];
        $user['id'] = $user['user_id'];
        unset($user['qualification_id']);
        unset($user['user_id']);
        $cities = $this->getUsersCityTable()->getUsersCity($user['id']);
        foreach($this->getEducationTable()->fetchAll() as $education) {
            if ($user['q_id'] == $education->qualification_id) {
                $user['qualification'] = $education->name;
            }
        }
        $citiesArr = array();
        foreach($cities as $user_city) {
            foreach($this->getCityTable()->fetchAll() as $city) {
                if ($user_city->city_id == $city->city_id) {
                    $citiesArr[] = $city->name;
                }
            }
        }
        $user['city'] = implode(',',$citiesArr);
        return new JsonModel(array(
            'data' => $user,
        ));
    }

    public function create($data)
    {
        $user = new Users();
        $user->user_id = 0;
        $user->name = $data['name'];
        $user->qualification_id = $data['qualification'];
        $this->getUsersTable()->saveUsers($user);
        if (isset($data['city'])) {
            $cities = explode(',',$data['city']);
            if (count($cities)>1) {
                foreach ($cities as $city) {
                    $users_city = new UsersCity();
                    $users_city->city_id = $city;
                    $users_city->user_id = $this->getUsersTable()->getLastId();
                    $this->getUsersCityTable()->saveUsersCity($users_city);
                }
            } else {
                $users_city = new UsersCity();
                $users_city->city_id = $data['city'];
                $users_city->user_id = $this->getUsersTable()->getLastId();
                $this->getUsersCityTable()->saveUsersCity($users_city);
            }
        }
        return new JsonModel(array(
            'data' => 'ok',
        ));
    }

    public function update($id, $data)
    {
        $user = new Users();
        $user->user_id = $id;
        $user->name = $data['name'];
        $user->qualification_id = $data['qualification'];
        $this->getUsersTable()->saveUsers($user);
        $this->getUsersCityTable()->deleteUsersCity($id);
        if (isset($data['city'])) {
            $cities = explode(',',$data['city']);
            if (count($cities)>1) {
                foreach ($cities as $city) {
                    $users_city = new UsersCity();
                    $users_city->city_id = $city;
                    $users_city->user_id = $id;
                    $this->getUsersCityTable()->saveUsersCity($users_city);
                }
            } else {
                $users_city = new UsersCity();
                $users_city->city_id = $data['city'];
                $users_city->user_id = $id;
                $this->getUsersCityTable()->saveUsersCity($users_city);
            }
        }
        return new JsonModel(array(
            'data' => 'ok',
        ));
    }

    public function delete($id)
    {
        $this->getUsersTable()->deleteUsers($id);
        return new JsonModel(array(
            'data' => 'ok',
        ));
    }

    public function getAdapter()
    {
        if (!$this->adapter) {
            $sm = $this->getServiceLocator();
            $this->adapter = $sm->get('Zend\Db\Adapter\Adapter');
        }
        return $this->adapter;
    }

    public function getUsersTable()
    {
        if (!$this->usersTable) {
            $sm = $this->getServiceLocator();
            $this->usersTable = $sm->get('Application\Model\UsersTable');
        }
        return $this->usersTable;
    }

    public function getCityTable()
    {
        if (!$this->cityTable) {
            $sm = $this->getServiceLocator();
            $this->cityTable = $sm->get('Application\Model\CityTable');
        }
        return $this->cityTable;
    }

    public function getEducationTable()
    {
        if (!$this->educationTable) {
            $sm = $this->getServiceLocator();
            $this->educationTable = $sm->get('Application\Model\EducationTable');
        }
        return $this->educationTable;
    }

    public function getUsersCityTable()
    {
        if (!$this->usersCityTable) {
            $sm = $this->getServiceLocator();
            $this->usersCityTable = $sm->get('Application\Model\UsersCityTable');
        }
        return $this->usersCityTable;
    }
}
